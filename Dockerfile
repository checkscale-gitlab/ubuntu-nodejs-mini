# ubuntu-nodejs-mini

ARG DOCKER_REGISTRY_URL=registry.gitlab.com/docking/
ARG CUSTOM_VERSION=1.0
ARG UBUNTU_VERSION=18.04
ARG NODEJS_VERSION=14.15.5

ARG NODEJS_PKG=nodejs_14.15.5-1nodesource1_amd64.deb
ARG NODEJS_URL=https://deb.nodesource.com/node_14.x/pool/main/n/nodejs/${NODEJS_PKG}


FROM ${DOCKER_REGISTRY_URL}ubuntu:${CUSTOM_VERSION}-${UBUNTU_VERSION} AS ubuntu-nodejs-mini

ARG NODEJS_PKG
ARG NODEJS_URL

WORKDIR /root

RUN \
	apt-get -q -y update ; \
	apt-get -q -y install \
		python-minimal \
	; \
	apt-get -q -y clean ; \
	find /var/lib/apt/lists/ -type f -delete

RUN \
	curl -Lo ${NODEJS_PKG} ${NODEJS_URL} \
		&& dpkg -i ${NODEJS_PKG} \
		&& rm ${NODEJS_PKG}
