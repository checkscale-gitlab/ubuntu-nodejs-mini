# ubuntu-nodejs-mini

all: help

.PHONY: all help build pull push shell

help:
	@echo "Available targets:"
	@echo
	@echo "  build      build and tag Docker image"
	@echo "  manifest   generate a list of packages & versions included in Docker image"
	@echo "  pull       pull Docker image"
	@echo "  push       push Docker image"
	@echo "  shell      run a shell in Docker image"
	@echo


CUSTOM_VERSION?=1.0
UBUNTU_VERSION?=18.04
NODEJS_VERSION?=14.15.5

NODEJS_PKG?=nodejs_14.15.5-1nodesource1_amd64.deb
NODEJS_URL?=https://deb.nodesource.com/node_14.x/pool/main/n/nodejs/$(NODEJS_PKG)

DOCKER_IMAGE:=ubuntu-nodejs-mini
DOCKER_REGISTRY_URL?=registry.gitlab.com/docking/
DOCKER_TAG?=$(CUSTOM_VERSION)-$(UBUNTU_VERSION)-$(NODEJS_VERSION)

build:
	docker build \
		--build-arg DOCKER_REGISTRY_URL=$(DOCKER_REGISTRY_URL) \
		--build-arg UBUNTU_VERSION=$(UBUNTU_VERSION) \
		--build-arg NODEJS_VERSION=$(NODEJS_VERSION) \
		--build-arg NODEJS_PKG=$(NODEJS_PKG) \
		--build-arg NODEJS_URL=$(NODEJS_URL) \
		--tag $(DOCKER_IMAGE):$(DOCKER_TAG) \
		$(if $(DOCKER_NO_CACHE),--no-cache,) \
		.

manifest:
	docker run \
		$(DOCKER_IMAGE):$(DOCKER_TAG) \
		dpkg-query --show --showformat='$${Package}=$${Version}\n' | sort \
		> ./Manifest.txt

pull:
	docker pull \
		$(DOCKER_REGISTRY_URL)$(DOCKER_IMAGE):$(DOCKER_TAG)
	docker tag \
		$(DOCKER_REGISTRY_URL)$(DOCKER_IMAGE):$(DOCKER_TAG) \
		$(DOCKER_IMAGE):$(DOCKER_TAG)

push:
	docker tag \
		$(DOCKER_IMAGE):$(DOCKER_TAG) \
		$(DOCKER_REGISTRY_URL)$(DOCKER_IMAGE):$(DOCKER_TAG)
	docker push \
		$(DOCKER_REGISTRY_URL)$(DOCKER_IMAGE):$(DOCKER_TAG)

shell:
	docker run \
		--interactive --tty \
		$(DOCKER_IMAGE):$(DOCKER_TAG) \
		bash
